<?php

Route::post('sign-in', 'Auth\LoginController@loginA');
Auth::routes();

//its worked use
//Route::group(array('domain' => '{account}.gesmansys.com'), function () {
//
//    Route::get('/', 'HomeController@subDomain');
//
//});


Route::group(['middleware' => ['auth', 'admin']], function () {

   
    Route::get('/', 'HomeController@redirect');


//    Route::group(['prefix' => 'admin', 'middleware' => 'roles', 'roles' => 'SU'], function () {
    Route::group(['prefix' => 'admin'], function () {

        Route::get('/', 'Admin\AdminController@index');
        Route::resource('/users', 'Admin\UsersController');
        Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
       
        Route::get('roles/datatable', 'Admin\RolesController@datatable');
        Route::resource('/roles', 'Admin\RolesController');
        Route::get('permissions/datatable', 'Admin\PermissionsController@datatable');
        Route::resource('/permissions', 'Admin\PermissionsController');
        
        Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
        Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
        Route::patch('/profile/edit', 'Admin\ProfileController@update');
        //
        Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
        Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');

        Route::resource('/category', 'Admin\CategoryController');
        Route::resource('/products', 'Admin\ProductController');
        Route::get('/products/create', 'Admin\ProductController@create');
        Route::post('/blogs/deleteimage', 'Admin\BlogController@deleteimage');
        Route::post('/blogs/deleteimage', 'Admin\BlogController@deleteimage');
        //Route::get('/categorydata', ['as' => 'CategoryControllerCategoryData', 'uses' => 'Admin\CategoryController@datatable']);

        Route::resource('/packages', 'Admin\PackageController');
        Route::get('/packages/create', 'Admin\PackageController@create');
        //Blog Module
        Route::resource('/blogs', 'Admin\BlogController');
        Route::get('/blogs/create', 'Admin\BlogController@create');

        Route::post('/packages/product', 'Admin\PackageController@productprice');
        Route::get('/opportunity', 'Admin\UsersController@opportunity');
    });

});

Route::get('/capsuleowner', function()
{
	
   Artisan::call('CapsuleownerCommand:capsuleownerCommand');

    //
});

Route::get('/getopportunity', function()
{
	
   Artisan::call('Getopportunity:getopportunity');

    //
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
