<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->longtext('shortdesc');
            $table->longtext('longdescription');
            $table->string('image');
            $table->string('video_type');
            $table->string('video_url');
            $table->string('page_title');
            $table->string('page_description');
            $table->string('meta_keyword');
            $table->string('og_title');
            $table->string('og_description');
            $table->string('status');
        
            $table->softDeletes();
            $table->timestamps();
        });
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('blogs');
    }
}
