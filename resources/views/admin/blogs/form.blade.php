<style>
        .imageClass_4.col-md-3 {
            margin: 0 0 0 110px;
        }
        .panel-default >.panel-heading
        {
            margin:0px !important;
        }
        ul.nav.nav-tabs {
            margin-bottom: 25px !important; 
            padding: 0px 10px 2px !important; 
        }
</style>
<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#step1">Step 1</a></li>
    <li><a data-toggle="tab" href="#step2">Step 2</a></li>
  </ul>
  <div class="tab-content">
    <div id="step1" class="tab-pane fade in active">
        <div class="col-md-6">
            <div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
                {!! Form::label('title', 'Title *', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('title', null, ['class' => 'form-control','required'=> 'required']) !!} {!! $errors->first('title', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
          
            <div class="form-group{{ $errors->has('video_type') ? ' has-error' : ''}}">
                {!! Form::label('video_type', 'Video Type', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::select('video_type',['youtube'=>'Youtube','vimeo'=>'Vimeo'],null, ['class' => 'form-control']) !!} {!! $errors->first('video_type',
                    '
                    <p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
            <div class="form-group{{ $errors->has('video_url') ? ' has-error' : ''}}">
                {!! Form::label('video_url', 'Video Link', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('video_url',null, ['class' => 'form-control']) !!} {!! $errors->first('video_url','
                    <p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
            <div class="form-group{{ $errors->has('page_title') ? ' has-error' : ''}}">
                {!! Form::label('page_title', 'Page Title', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('page_title',null, ['class' => 'form-control']) !!} {!! $errors->first('page_title',
                    '
                    <p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                {!! Form::label('status', 'Status', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::select('status',['' =>'--select--', '1'=>'Active','0'=>'Inactive'] ,null, ['class' => 'form-control']) !!} {!! $errors->first('status',
                    '
                    <p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
           
            <div class="form-group {{ $errors->has('meta_keyword') ? 'has-error' : ''}}">
                {!! Form::label('meta_keyword', 'Keyword', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::textarea('meta_keyword', null, ['class' => 'form-control','size' => '30x5']) !!} {!! $errors->first('meta_keyword',
                    '
                    <p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
            <div class="form-group{{ $errors->has('og_title') ? ' has-error' : ''}}">
                {!! Form::label('og_title', 'Og Title *', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('og_title',null, ['class' => 'form-control','required'=>'required']) !!} {!! $errors->first('og_title', '
                    <p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
        </div>
        
        <div class="col-md-6">
            @if(isset($blog) && $blog->image)
        
            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Change Feature Image', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <input type="file" name="image" id="image"> {!! $errors->first('image', '
                    <p class="help-block with-errors">:message</p>') !!}
                </div>
                <div>
                    <img src="{!! asset('Blogs/'.$blog->image) !!}" alt="" width="60px">
                </div>
            </div>
            @else
            <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                {!! Form::label('image', 'Feature Image *', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <input type="file" name="image" id="image"> {!! $errors->first('image', '
                    <p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
            @endif
            @if(isset($images) && $images)
            <div class="form-group {{ $errors->has('mimage') ? 'has-error' : ''}}">
                {!! Form::label('mimage', 'Multiple Images', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <input type="file" name="mimage[]" id="mimage" multiple> {!! $errors->first('mimage', '
                    <p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
            
            <div class="col-md-12">
                @foreach($images as $img_name)
                <?php $class="imageClass_".$img_name->id ?>
                <div class="{{$class}} col-md-3">
                    <img src="{!! asset('Blogs/'.$img_name->image) !!}" alt="" width="100%"> {!! Form::button('Delete', ['class'
                    => 'btn btn-primary btn-sm delete_image','img'=>$img_name->id]) !!}</br>
                    </br>
                </div>
                @endforeach
            </div>
            @else
            <div class="form-group {{ $errors->has('mimage') ? 'has-error' : ''}}">
                {!! Form::label('mimage', 'Multiple Images', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    <input type="file" name="mimage[]" id="mimage" multiple> {!! $errors->first('mimage', '
                    <p class="help-block with-errors">:message</p>') !!}
                </div>
            </div>
            @endif
        </div>
        
    </div>
    <div id="step2" class="tab-pane fade">
        <div class="col-md-12">
            <div class="form-group {{ $errors->has('shortdesc') ? 'has-error' : ''}}">
                  {!! Form::label('shortdesc', 'Short Description *', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::textarea('shortdesc', null, ['class' => 'form-control summernote','required'=> 'required']) !!} {!! $errors->first('shortdesc',
                      '
                      <p class="help-block with-errors">:message</p>') !!}
                  </div>
              </div>
              <div class="form-group {{ $errors->has('longdescription') ? 'has-error' : ''}}">
                  {!! Form::label('longdescription', 'Long Description', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::textarea('longdescription', null, ['class' => 'form-control summernote']) !!} {!! $errors->first('longdescription',
                      '
                      <p class="help-block with-errors">:message</p>') !!}
                  </div>
              </div>
               <div class="form-group {{ $errors->has('page_description') ? 'has-error' : ''}}">
                  {!! Form::label('page_description', 'Page Description', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::textarea('page_description', null, ['class' => 'form-control summernote']) !!} {!! $errors->first('page_description',
                      '
                      <p class="help-block with-errors">:message</p>') !!}
                  </div>
              </div>
                <div class="form-group {{ $errors->has('og_description') ? 'has-error' : ''}}">
                  {!! Form::label('og_description', 'Og Description', ['class' => 'col-md-2 control-label']) !!}
                  <div class="col-md-10">
                      {!! Form::textarea('og_description', null, ['class' => 'form-control summernote']) !!} {!! $errors->first('og_description',
                      '
                      <p class="help-block with-errors">:message</p>') !!}
                  </div>
              </div>
              </div>
                <div class="form-group">
                    <div class="col-md-offset-8 col-md-4">
                        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
            
    </div>
  </div>




@push('js')
<script>
    $('.delete_image').click(function(){
            var img_id = $(this).attr("img");
            //alert(img_id);
            $.ajax({
					type: "POST",
					url: '{{url("admin/blogs/deleteimage/")}}',
					data: {id:img_id },
					success: function( msg ) {
                        $('.imageClass_'+img_id).hide();                       
					}
				});
        });
     
</script>
@endpush