@extends('layouts.backend') 
@section('title','Blogs') 
@section('pageTitle','Blogs') 
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <i class="icon-circle-blank"></i> Blogs
                </div>
            </div>
            <div class="box-content ">

                <div class="row">
                    <div class="col-md-6">
                        <a href="{{ url('/admin/blogs/create') }}" class="btn btn-success btn-sm" title="Add New Document">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New Blog
                                </a>
                    </div>
                    <div class="col-md-6">
                        {!! Form::open(['method' => 'GET', 'url' => '/admin/blogs', 'class' => 'navbar-form navbar-right', 'role' => 'search']) !!}
                        <input type="search" class="form-control search" name="search" placeholder="{{Request::get('search')}}" value="{!! request()->get('search') !!}">                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-borderless" id="products-table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Video Type</th>
                                <th>Video Url</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($blogs as $blogdata)
                            <tr>
                                <td> {{$blogdata->id}}</td>
                                <td> {{$blogdata->title}}</td>
                                <td>
                                    @if($blogdata->image)
                                    <img src="{!! asset('Blogs/'.$blogdata->image) !!}" style="height:50px;width:50px;">                                    </td>
                                @else @endif
                                <td>{{$blogdata->video_type}}</td>
                                <td>{{$blogdata->video_url}}</td>
                                <td>
                                    <a href="{{ url('/admin/blogs/' . $blogdata->id) }}" title="View product">
                                                    <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                           aria-hidden="true"></i> View
                                                    </button>
                                                </a> {{-- @if(Auth::user()->can('access.user.edit'))
                                    --}}
                                    <a href="{{ url('/admin/blogs/' . $blogdata->id . '/edit') }}" title="Edit product">
                                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                                  aria-hidden="true"></i> Edit
                                                        </button>
                                                    </a> {{-- @endif --}} 
                                {{--  @if(Auth::user()->can('access.blogs'))  --}}
                                    {!! Form::open([ 'method' => 'DELETE', 'url' => ['/admin/blogs', $blogdata->id], 'style'
                                    => 'display:inline' ]) !!} {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>                                    Delete', array( 'type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'title' => 'Delete
                                    blog', 'onclick'=>'return confirm("Confirm delete?")' )) !!} {!! Form::close() !!}
                                    {{--  @endif  --}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="pagination-wrapper"> {!! $blogs->appends(['search' => Request::get('search')])->render() !!} </div>
            </div>
        </div>
    </div>
</div>
@endsection