@extends('layouts.backend')

@section('title','Products')
@section('pageTitle','Products')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                      Products
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ url('/admin/products/create') }}" class="btn btn-success btn-sm"
                                   title="Add New Document">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New Product
                                </a>

                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/products', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                            <input type="search" class="form-control search" name="search" placeholder="{{Request::get('search')}}" value="{!! request()->get('search') !!}">
                            
                            {!! Form::close() !!}
                            </div>
                        </div>
                    


                    <div class="table-responsive">
                        <table class="table table-borderless" id="products-table">
                            <thead>
                            <tr>
                                <th>Id</th>                         
                                <th>Name</th>
                                <th>Image</th>   
                                <th>Category</th>
                                <th>Price</th>
                                <th>Sale Price</th>
                                <th>Actions</th>                        
                            </tr>
                            </thead>
                            <tbody>
                                    @foreach($products as $item)
                                    
                                    <tr>
                                        <td> {{$item->id}}</td>
                                        <td> {{$item->name}}</td>
                                        <td>
                                           @if($item->image) 
                                            <img src="{!! asset('Products/'.$item->image) !!}" style="height:50px;width:50px;"> </td>
                                            @else
                                            @endif
                                            <td>{{$item->cat_name}}</td>
                                        <td>{{$item->price}}</td>
                                        <td>{{$item->sale_price}}</td>
                                        <td>
        
                                                <a href="{{ url('/admin/products/' . $item->id) }}" title="View product">
                                                    <button class="btn btn-info btn-xs"><i class="fa fa-eye"
                                                                                           aria-hidden="true"></i> View
                                                    </button>
                                                </a>
        
                                                {{--  @if(Auth::user()->can('access.user.edit'))  --}}
        
                                                    <a href="{{ url('/admin/products/' . $item->id . '/edit') }}"
                                                       title="Edit product">
                                                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                                                  aria-hidden="true"></i> Edit
                                                        </button>
                                                    </a>
                                                {{--  @endif  --}}
        
                                                @if(Auth::user()->can('access.product.delete'))
        
                                                    {!! Form::open([
                                                        'method' => 'DELETE',
                                                        'url' => ['/admin/products', $item->id],
                                                        'style' => 'display:inline'
                                                    ]) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                            'type' => 'submit',
                                                            'class' => 'btn btn-danger btn-xs',
                                                            'title' => 'Delete product',
                                                            'onclick'=>'return confirm("Confirm delete?")'
                                                    )) !!}
                                                    {!! Form::close() !!}
                                                @endif
        
                                            </td>
                                    </tr>   
                                    @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pagination-wrapper"> {!! $products->appends(['search' => Request::get('search')])->render() !!} </div>
                </div>
            </div>
        </div>
    </div>
@endsection

