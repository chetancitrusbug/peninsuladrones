
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Name *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control','required'=> 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description *', ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
    {!! Form::textarea('description', null, ['class' => 'form-control','size' => '30x5','required'=>
    'required']) !!}
    {!! $errors->first('description', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('shortdesc') ? 'has-error' : ''}}">
        {!! Form::label('shortdesc', 'Short Description', ['class' => 'col-md-4 control-label']) !!}
         <div class="col-md-6">
        {!! Form::textarea('shortdesc', null, ['class' => 'form-control','size' => '30x5']) !!}
        {!! $errors->first('shortdesc', '<p class="help-block with-errors">:message</p>') !!}
        </div>
    </div>
<div class="form-group{{ $errors->has('category_id') ? ' has-error' : ''}}">
    {!! Form::label('category_id', 'Category', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        
        {!! Form::select('category_id',$category, null, ['class' => 'form-control']) !!}
		{!! $errors->first('category_id', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('price') ? ' has-error' : ''}}">
    {!! Form::label('price', 'Price *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        
        {!! Form::text('price',null, ['class' => 'form-control','required'=>'required']) !!}
        {!! $errors->first('price', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('sale_price') ? ' has-error' : ''}}">
        {!! Form::label('sale_price', 'Sale Price *', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            
            {!! Form::text('sale_price',null, ['class' => 'form-control','required'=>'required']) !!}
            {!! $errors->first('sale_price', '<p class="help-block with-errors">:message</p>') !!}
        </div>
    </div>

@if(isset($product) && $product->image)


<img src="{!! asset('Products/'.$product->image) !!}" alt="" width="100px">

<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image',  'Change Feature Image', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <input type="file" name="image" id="image">
            {!! $errors->first('image', '<p class="help-block with-errors">:message</p>') !!}
        </div>
    </div>
@else
<div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
    {!! Form::label('image',  'Feature Image *', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <input type="file" name="image" id="image">
            {!! $errors->first('image', '<p class="help-block with-errors">:message</p>') !!}
        </div>
    </div>
    
@endif

@if(isset($images) && $images)
@foreach($images as $img_name)
<?php $class="imageClass_".$img_name->id ?>
<div class={{$class}} >
<img src="{!! asset('Products/'.$img_name->image) !!}" alt="" width="100px">
{!! Form::button('Delete', ['class' => 'btn btn-primary delete_image','img'=>$img_name->id]) !!}</br></br>
</div>
@endforeach
<div class="form-group {{ $errors->has('mimage') ? 'has-error' : ''}}">
    {!! Form::label('mimage',  'Change Images', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <input type="file" name="mimage[]" id="mimage" multiple >
            {!! $errors->first('mimage', '<p class="help-block with-errors">:message</p>') !!}
        </div>
    </div>
@else
<div class="form-group {{ $errors->has('mimage') ? 'has-error' : ''}}">
{!! Form::label('mimage',  'Other Images', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <input type="file" name="mimage[]" id="mimage" multiple >
        {!! $errors->first('mimage', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
    
@endif







<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@push('js')
    <script>
        $('.delete_image').click(function(){
            var img_id = $(this).attr("img");
            //alert(img_id);
            $.ajax({
					type: "POST",
					url: '{{url("admin/products/deleteimage/")}}',
					data: {id:img_id },
					success: function( msg ) {
                        $('.imageClass_'+img_id).hide();                       
					}
				});
        });
        </script>
@endpush

