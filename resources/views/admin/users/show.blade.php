@extends('layouts.backend') 
@section('title', 'Show User') 
@section('pageTitle','Datail Of User')

@section('content')

<div class="row">

    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <i class="icon-circle-blank"></i>
                    Show User &nbsp;&nbsp; #{{$user->name}}
                </div>
                {{--  <div class="actions">
                    @include('partials.page_tooltip',['model' => 'user','page'=>'form'])
                </div>  --}}

            </div>
            <div class="box-content panel-body">
                <a href="{{ url('/admin/users') }}" title="Back" class="pull-right">
                    <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </button>
                </a>
               
                @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Edit User">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                           edit
                        </button>
                    </a>
                @endif
                 {!! Form::open([ 'method' => 'DELETE', 'url' => ['/admin/users', $user->id],
                    'style' => 'display:inline' ]) !!} {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>
                    Delete', array( 'type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'title' => 'Delete User', 'onclick'=>"return
                    confirm('Confirm delete?')" ))!!} {!! Form::close() !!}
                   
                    <br/>
                    <br/>

                    <?php
                    $role = join(' + ', $user->roles()->pluck('label')->toArray());
                    ?>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-borderless">
                                    <tbody>
                                        <tr>
                                            <td> Id </td>
                                            <td>{{ $user->id }}</td>
                                        </tr>

                                        <tr>
                                            <td> Name </td>
                                            <td>{{ $user->name }}</td>
                                        </tr>


                                        <tr>
                                            <td> Email </td>
                                            <td>{{ $user->email }}</td>
                                        </tr>
                                        <tr>
                                            <td>Role </td>
                                            <td>{{ $role }}</td>
                                        </tr>



                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    

@endsection