<?php 
if(isset($package->package_product) && $package->package_product != '' ){
    foreach($package->package_product as $product){
        $array[] = $product->id;
    }   
}else{
    $array = array();
}
?>
<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', 'Title *', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control','required'=> 'required']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('product') ? ' has-error' : ''}}">
    {!! Form::label('product', 'Product', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6"> 
            {{--  <select name="product[]" class="form-control select2" id='product' multiple>
                @foreach($product as $item)
                    <option value="{{$item->id}}" price="{{$item->price}}">{{$item->name}}</option>
                @endforeach   
            </select>   --}}
            {{Form::select('product[]',$productArray, $array,['class'=>'form-control select2','id'=>'product','multiple'])}}     
        {{--  {!! Form::select('product[]',$product, null, ['class' => 'form-control select2','id'=>'product','multiple']) !!}  --}}
		{!! $errors->first('product', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>




<div class="form-group{{ $errors->has('price') ? ' has-error' : ''}}">
    {!! Form::label('price', 'Price *', ['class' => 'col-md-4 control-label price']) !!}
    <div class="col-md-6">
        
        {!! Form::text('price',null, ['class' => 'form-control price','readonly']) !!}
        {!! $errors->first('price', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('kw') ? ' has-error' : ''}}">
    {!! Form::label('kw', 'KW', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('kw',null, ['class' => 'form-control']) !!}
        {!! $errors->first('kw', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>

@push('js')
<script type="text/javascript">
    $product =  $("#product").val();
   
   //$('.price').attr("disabled","disabled") 
    $("#product").change(function(){
        var product_id = $(this).val();        
        var req = {
            'product_id': product_id,
        };
        $.ajax({
            url: '{{url('admin/packages/product')}}',
            method: 'POST',
            data: req,
            success: function (data) {
                // $(process_model_id).modal('hide');
                var result = JSON.parse(data);
                $('.price').val(result.finalprice);
            },            
        });
        return false;
    });
    
</script>
@endpush

