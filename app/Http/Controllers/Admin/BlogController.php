<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use App\BlogImages;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $blogs = Blog::select('blogs.*')
            ->where('status', 1)
            ->where('blogs.title', 'LIKE', "%$keyword%")
            ->orWhere('blogs.video_type', 'LIKE', "%$keyword%")
            ->paginate($perPage);
        } else {
            $blogs =  Blog::where('status',1)
           ->paginate($perPage);
        }                      
        return view('admin.blogs.index',compact('blogs'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {  
       // $category = Category::pluck('name','id')->prepend('Select Category',''); 
		return view('admin.blogs.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
		$this->validate($request, [
            'title' => 'required',
            'shortdesc' => 'required',
            'longdescription' => 'required',
            'video_type' => 'required',
            'video_url' => 'required',
            'page_title'=>'required',
            'page_description'=>'required',
            'og_title'=>'required',
            'og_description'=>'required'
            //'image' => 'required|mimes:jpeg,jpg,png | max:100'
        ]);
		$data = $request->all();
       
       
        
        if ($request->file('image')) {
            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('Blogs'), $filename);
            $data['image'] = $filename;
        }
        $blog = Blog::create($data);

        $files = $request->file('mimage');
        if ($request->file('mimage')) {
            foreach ($files as $image) {                
                $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('Blogs'), $filename);
                $dataimage['image']=$filename;
                $dataimage['blog_id'] = $blog->id; 
                $image = BlogImages::create($dataimage);
               
            }
        
        }
        
        
       Session::flash('flash_message', 'Blog added!');
        return redirect('admin/blogs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {  
        $blog = Blog::where('status', 1)->where('id',$id)->first(); 
        if($blog)
        {
            $images = BlogImages::where('blog_id',$id)->get();
            return view('admin.blogs.show', compact('blog','images'));
            
        }
        else{
            Session::flash('flash_error', 'Blog is not exist!');
            return redirect('admin/blogs');
        }
        
    }
    public function edit(Request $request,$id)
    {
        $blog = Blog::where('status', 1)->where('id',$id)->first();
        if($blog)
        {
            $images = BlogImages::where('blog_id',$id)->get();
            return view('admin.blogs.edit', compact('blog','images'));
        }
        else{
            Session::flash('flash_error', 'Blog is not exist!');
            return redirect('admin/blogs');
        }
    }
    
     
 /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
      //  dd($request);
        $this->validate($request,[            
            'title' => 'required',
            'shortdesc' => 'required',
            'longdescription' => 'required',
            'video_type' => 'required',
            'video_url' => 'required',
            'page_title'=>'required',
            'page_description'=>'required',
            'og_title'=>'required',
            'og_description'=>'required'
        ]);
        $requestData = $request->all(); 
      
        $Blog = Blog::where('status',1)->findOrFail($id);

        if ($request->file('image')) {
            $fimage = $request->file('image');           
            $filename = uniqid(time()) . '.' . $fimage->getClientOriginalExtension();
            $fimage->move(public_path('Blogs'), $filename);
            $requestData['image'] = $filename;
        }    


        $files = $request->file('mimage');
        if ($request->file('mimage')) {        
            if(count($files)){
                $images = BlogImages::where('blog_id',$id);
                $images->delete();
                foreach ($files as $image) {                    
                    $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('Blogs'), $filename);
                
                    $imageData['image']=$filename;
                    $imageData['blog_id'] = $id;
                    $image = BlogImages::create($imageData);
                }
            }
        }
        
	    $Blog->update($requestData);

        flash('Blog Updated Successfully!');
		
        return redirect('admin/blogs');
    }

        
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
       
        $blog = Blog::where('status',1)->find($id);
        unlink(public_path('Blogs') . '/'.$blog->image);
        $blog->delete();

        $images = BlogImages::where('blog_id',$id);
        $images->delete();

        if($request->ajax()){
            $message='Deleted';
             return response()->json(['message'=>$message],200);
        }else{
            Session::flash('flash_message','Blog Deleted Successfully!');            
            return redirect('admin/blogs');
        }
       
    }  


    public function deleteimage(Request $request)
    {
        $id =  $request->id;
        $image = BlogImages::where('id',$id)->first();
        $image->delete();
        $JSONARRAY = Array(
            'msg'=>'Success',
        );
        // echo json_encode($JSONARRAY);
        exit;
    }     

          

}
