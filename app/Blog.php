<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Blog extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blogs';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'title', 'shortdesc','longdescription','image','video_type', 'video_url','page_title','page_description','meta_keyword','og_title','og_description','status','deleted_at'];
	 public function category(){
		return $this->hasMany('App\Category','id');
	} 
}
